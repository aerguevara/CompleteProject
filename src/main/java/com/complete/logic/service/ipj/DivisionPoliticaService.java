package com.complete.logic.service.ipj;

import com.complete.modelo.entity.catalogo.DivisionPolitica;

import java.util.List;

/**
 * Created by Angelo on 24/09/2016.
 */
public interface DivisionPoliticaService {

    List<DivisionPolitica> obtenerListaDepartamentos();
    List<DivisionPolitica> obtenerListaMunicipios(int idDepartamento);
}
