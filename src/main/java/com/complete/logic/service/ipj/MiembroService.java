package com.complete.logic.service.ipj;

import com.complete.modelo.dao.DAOException;
import com.complete.modelo.entity.ipj.Miembro;

import java.util.List;

/**
 * Created by Angelo on 24/09/2016.
 */
public interface MiembroService {
    List<Miembro> obtenerMiembrosPastoresPorIglesia(int iglesia);
    void guardar(Miembro miembro) throws DAOException;
    void actualizar(Miembro miembro) throws DAOException;
}
