package com.complete.logic.service.ipj;

import com.complete.modelo.entity.catalogo.Catalogo;

import java.util.List;

/**
 * Created by Angelo on 26/09/2016.
 */
public interface CatalogoService {

    List<Catalogo> obtenerCatalogoPorTipoCatalogo(String codigo);

    Catalogo obtenerCatalogoPorCodigo(String codigo);
}
