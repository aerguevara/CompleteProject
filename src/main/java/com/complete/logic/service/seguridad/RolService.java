package com.complete.logic.service.seguridad;

import com.complete.modelo.entity.seguridad.Rol;

import java.util.List;

/**
 * Created by Angelo on 15/09/2016.
 */
public interface RolService {

    List<Rol> buscarRolPorUsuarioId(int id);
}
