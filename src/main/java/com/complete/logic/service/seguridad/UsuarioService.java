package com.complete.logic.service.seguridad;

import com.complete.modelo.entity.seguridad.Usuario;

/**
 * Created by Angelo on 04/09/2016.
 */
public interface UsuarioService {

    Usuario BuscarUsuario(String nombre);
}
