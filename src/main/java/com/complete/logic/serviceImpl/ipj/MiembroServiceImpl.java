package com.complete.logic.serviceImpl.ipj;

import com.complete.logic.service.ipj.MiembroService;
import com.complete.modelo.dao.DAOException;
import com.complete.modelo.dao.ipj.MiembroDAO;
import com.complete.modelo.entity.ipj.Miembro;
import com.googlecode.genericdao.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Angelo on 24/09/2016.
 */
@Service
public class MiembroServiceImpl implements MiembroService {

    @Autowired
    MiembroDAO miembroDAO;

    @Transactional
   public List<Miembro> obtenerMiembrosPastoresPorIglesia(int iglesia){
        Search oSearch = new Search();
        oSearch.addFilterEqual("tipoMiembro.codigo","001");
        oSearch.addFilterEqual("idIglesia.id",iglesia);
        return miembroDAO.search(oSearch);

    }

    @Transactional
    public void guardar(Miembro miembro) throws DAOException {
        miembroDAO.saveUpper(miembro);
    }

    @Transactional
    public void actualizar(Miembro miembro) throws DAOException {
        miembroDAO.updateUpper(miembro);

    }

}
