package com.complete.logic.serviceImpl.ipj;

import com.complete.logic.service.ipj.CatalogoService;
import com.complete.modelo.dao.ipj.CatalogoDAO;
import com.complete.modelo.entity.catalogo.Catalogo;
import com.googlecode.genericdao.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Angelo on 26/09/2016.
 */
@Service
public class CatalogoServiceImpl implements CatalogoService {

    @Autowired
    CatalogoDAO catalogoDAO;


    @Transactional
    public List<Catalogo> obtenerCatalogoPorTipoCatalogo(String codigo) {
        Search oSearch = new Search();
        oSearch.addFilterEqual("refTipoCatalogo.codigo",codigo);
        return catalogoDAO.search(oSearch);

    }

    @Transactional
    public Catalogo obtenerCatalogoPorCodigo(String codigo) {
        Catalogo cat = null;
        Search oSearch = new Search();
        oSearch.addFilterEqual("codigo",codigo);

        List<Catalogo> listaCatalogo= catalogoDAO.search(oSearch);

        if(listaCatalogo.size()>0){
            cat = listaCatalogo.get(0);
        }

        return cat;

    }
}
