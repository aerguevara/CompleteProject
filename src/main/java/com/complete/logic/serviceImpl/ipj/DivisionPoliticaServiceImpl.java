package com.complete.logic.serviceImpl.ipj;

import com.complete.logic.service.ipj.DivisionPoliticaService;
import com.complete.modelo.dao.ipj.DivisionPoliticaDAO;
import com.complete.modelo.entity.catalogo.DivisionPolitica;
import com.googlecode.genericdao.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Angelo on 24/09/2016.
 */
@Service
public class DivisionPoliticaServiceImpl implements DivisionPoliticaService{

    @Autowired
    DivisionPoliticaDAO divisionPoliticaDAO;

    @Transactional
    public List<DivisionPolitica> obtenerListaDepartamentos() {
        Search oSearch = new Search();
        oSearch.addFilterNull("divisionPoliticiaId");
         return divisionPoliticaDAO.search(oSearch);
    }

    @Transactional
    public List<DivisionPolitica> obtenerListaMunicipios(int idDepartamento) {
        Search oSearch = new Search();
        oSearch.addFilterEqual("divisionPoliticiaId.id",idDepartamento);
        return divisionPoliticaDAO.search(oSearch);
    }
}
