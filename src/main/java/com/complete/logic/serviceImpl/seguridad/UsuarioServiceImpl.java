package com.complete.logic.serviceImpl.seguridad;

import com.complete.logic.service.seguridad.UsuarioService;
import com.complete.modelo.dao.seguridad.UsuarioDAO;
import com.complete.modelo.entity.seguridad.Usuario;
import com.googlecode.genericdao.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Angelo on 04/09/2016.
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    UsuarioDAO oUsuarioDAO;

    @Transactional
    public Usuario BuscarUsuario(String nombre){
        Search oSearch = new Search();
        oSearch.addFilterEqual("nombre",nombre);

        List<Usuario> listaUsuario = oUsuarioDAO.search(oSearch);
        Usuario oUsuario = null;

        if(listaUsuario.size()>0){
            oUsuario = listaUsuario.get(0);
        }
        return oUsuario;
    }
}
