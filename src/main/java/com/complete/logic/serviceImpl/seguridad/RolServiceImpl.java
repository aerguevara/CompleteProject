package com.complete.logic.serviceImpl.seguridad;

import com.complete.logic.service.seguridad.RolService;
import com.complete.modelo.dao.seguridad.RolDAO;
import com.complete.modelo.entity.seguridad.Rol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Angelo on 15/09/2016.
 */
@Service
public class RolServiceImpl implements RolService {
    @Autowired
    RolDAO oRolDAO;

    @Transactional
    public List<Rol> buscarRolPorUsuarioId(int id) {
        return oRolDAO.buscarRolPorUsuarioId(id);
    }
}
