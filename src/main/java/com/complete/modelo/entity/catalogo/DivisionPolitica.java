package com.complete.modelo.entity.catalogo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Angelo on 21/05/2016.
 */
@Entity
@Table(name = "division_politica", schema = "catalogo", catalog = "ipj_desarrollo")
public class DivisionPolitica implements Serializable {
    private Integer id;
    private String nombre;
    private String codigo;
    private DivisionPolitica divisionPoliticiaId;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


    @ManyToOne
    @JoinColumn(name = "division_politicia_id",referencedColumnName = "id")
    public DivisionPolitica getDivisionPoliticiaId() {
        return divisionPoliticiaId;
    }

    public void setDivisionPoliticiaId(DivisionPolitica divisionPoliticiaId) {
        this.divisionPoliticiaId = divisionPoliticiaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DivisionPolitica that = (DivisionPolitica) o;

        if (codigo != null ? !codigo.equals(that.codigo) : that.codigo != null) return false;
        if (divisionPoliticiaId != null ? !divisionPoliticiaId.equals(that.divisionPoliticiaId) : that.divisionPoliticiaId != null)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (codigo != null ? codigo.hashCode() : 0);
        result = 31 * result + (divisionPoliticiaId != null ? divisionPoliticiaId.hashCode() : 0);
        return result;
    }
}
