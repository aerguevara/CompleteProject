package com.complete.modelo.entity.catalogo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Angelo on 21/05/2016.
 */
@Entity
@Table(name = "catalogo", schema = "catalogo", catalog = "ipj_desarrollo")
public class Catalogo implements Serializable {

    private Integer id;
    private String nombre;
    private String descripcion;
    private String codigo;
    private Integer orden;
    private TipoCatalogo refTipoCatalogo;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "orden")
    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Catalogo catalogo = (Catalogo) o;

        if (codigo != null ? !codigo.equals(catalogo.codigo) : catalogo.codigo != null) return false;
        if (descripcion != null ? !descripcion.equals(catalogo.descripcion) : catalogo.descripcion != null)
            return false;
        if (id != null ? !id.equals(catalogo.id) : catalogo.id != null) return false;
        if (nombre != null ? !nombre.equals(catalogo.nombre) : catalogo.nombre != null) return false;
        if (orden != null ? !orden.equals(catalogo.orden) : catalogo.orden != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (codigo != null ? codigo.hashCode() : 0);
        result = 31 * result + (orden != null ? orden.hashCode() : 0);
        return result;
    }
    @ManyToOne
    @JoinColumn(name = "ref_tipo_catalogo",referencedColumnName = "codigo")
    public TipoCatalogo getRefTipoCatalogo() {
        return refTipoCatalogo;
    }

    public void setRefTipoCatalogo(TipoCatalogo refTipoCatalogo) {
        this.refTipoCatalogo = refTipoCatalogo;
    }
}
