package com.complete.modelo.entity.ipj;



import com.complete.modelo.entity.catalogo.Catalogo;
import com.complete.modelo.entity.seguridad.Usuario;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Angelo on 21/05/2016.
 */
@Entity
@Table(name = "estado_financiero", schema = "ipj", catalog = "ipj_desarrollo")
public class EstadoFinanciero implements Serializable {
    private Integer id;
    private Timestamp fecha;
    private String descripcion;
    private Integer monto;
    private Entidad entidadId;
    private Catalogo refTipo;
    private Usuario usuarioRegistra;
    private Usuario usuarioActualiza;
    private Timestamp fechaRegistra;
    private Timestamp fechaModifica;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha")
    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "monto")
    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EstadoFinanciero that = (EstadoFinanciero) o;

        if (descripcion != null ? !descripcion.equals(that.descripcion) : that.descripcion != null) return false;
        if (fecha != null ? !fecha.equals(that.fecha) : that.fecha != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (monto != null ? !monto.equals(that.monto) : that.monto != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (fecha != null ? fecha.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (monto != null ? monto.hashCode() : 0);
        return result;
    }
    @ManyToOne
    @JoinColumn(name = "entidad_id",referencedColumnName = "id")
    public Entidad getEntidadId() {
        return entidadId;
    }

    public void setEntidadId(Entidad entidadId) {
        this.entidadId = entidadId;
    }

    @ManyToOne
    @JoinColumn(name = "ref_tipo",referencedColumnName = "codigo")
    public Catalogo getRefTipo() {
        return refTipo;
    }

    public void setRefTipo(Catalogo refTipo) {
        this.refTipo = refTipo;
    }

    @ManyToOne
    @JoinColumn(name = "usuario_registra",referencedColumnName = "id")
    public Usuario getUsuarioRegistra() {
        return usuarioRegistra;
    }

    public void setUsuarioRegistra(Usuario usuarioRegistra) {
        this.usuarioRegistra = usuarioRegistra;
    }
    @ManyToOne
    @JoinColumn(name = "usuario_actualiza",referencedColumnName = "id")
    public Usuario getUsuarioActualiza() {
        return usuarioActualiza;
    }

    public void setUsuarioActualiza(Usuario usuarioActualiza) {
        this.usuarioActualiza = usuarioActualiza;
    }
    @Basic
    @Column(name = "fecha_registra")
    public Timestamp getFechaRegistra() {
        return fechaRegistra;
    }

    public void setFechaRegistra(Timestamp fechaRegistra) {
        this.fechaRegistra = fechaRegistra;
    }
    @Basic
    @Column(name = "fecha_actualiza")
    public Timestamp getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Timestamp fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

}
