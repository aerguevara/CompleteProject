package com.complete.modelo.entity.ipj;

import com.complete.modelo.entity.catalogo.Catalogo;
import com.complete.modelo.entity.seguridad.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Angelo on 27/09/2016.
 */
@Entity
@Table(name = "miembro_entidad", schema = "ipj", catalog = "ipj_desarrollo")
@SequenceGenerator(name = "miembro_entidad_id_seq", sequenceName = "ipj.miembro_entidad_id_seq", allocationSize = 1)
public class MiembroEntidad implements Serializable {

    private Integer id;
    private Entidad entidadId;
    private Miembro miembroId;
    private Catalogo refTipoMiembro;
    private Boolean estado;
    private Timestamp fechaRegistra;
    private Usuario usuarioRegistra;
    private Timestamp fechaActualiza;
    private Usuario usuarioActualiza;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "miembro_entidad_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "entidad_id",referencedColumnName = "id")
    public Entidad getEntidadId() {
        return entidadId;
    }

    public void setEntidadId(Entidad entidadId) {
        this.entidadId = entidadId;
    }

    @ManyToOne
    @JoinColumn(name = "miembro_id",referencedColumnName = "id")
    public Miembro getMiembroId() {
        return miembroId;
    }

    public void setMiembroId(Miembro miembroId) {
        this.miembroId = miembroId;
    }

    @ManyToOne
    @JoinColumn(name = "ref_tipo_miembro",referencedColumnName = "codigo")
    public Catalogo getRefTipoMiembro() {
        return refTipoMiembro;
    }

    public void setRefTipoMiembro(Catalogo refTipoMiembro) {
        this.refTipoMiembro = refTipoMiembro;
    }

    @Basic
    @Column(name = "estado")
    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Basic
    @Column(name = "fecha_registra")
    public Timestamp getFechaRegistra() {
        return fechaRegistra;
    }

    public void setFechaRegistra(Timestamp fechaRegistra) {
        this.fechaRegistra = fechaRegistra;
    }

    @Basic
    @Column(name = "fecha_actualiza")
    public Timestamp getFechaActualiza() {
        return fechaActualiza;
    }

    public void setFechaActualiza(Timestamp fechaActualiza) {
        this.fechaActualiza = fechaActualiza;
    }

    @ManyToOne
    @JoinColumn(name = "usuario_registra",referencedColumnName = "id")
    public Usuario getUsuarioRegistra() {
        return usuarioRegistra;
    }

    public void setUsuarioRegistra(Usuario usuarioRegistra) {
        this.usuarioRegistra = usuarioRegistra;
    }

    @ManyToOne
    @JoinColumn(name = "usuario_actualiza",referencedColumnName = "id")
    public Usuario getUsuarioActualiza() {
        return usuarioActualiza;
    }

    public void setUsuarioActualiza(Usuario usuarioActualiza) {
        this.usuarioActualiza = usuarioActualiza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MiembroEntidad that = (MiembroEntidad) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (entidadId != null ? !entidadId.equals(that.entidadId) : that.entidadId != null) return false;
        if (miembroId != null ? !miembroId.equals(that.miembroId) : that.miembroId != null) return false;
        if (refTipoMiembro != null ? !refTipoMiembro.equals(that.refTipoMiembro) : that.refTipoMiembro != null)
            return false;
        if (estado != null ? !estado.equals(that.estado) : that.estado != null) return false;
        if (fechaRegistra != null ? !fechaRegistra.equals(that.fechaRegistra) : that.fechaRegistra != null)
            return false;
        if (fechaActualiza != null ? !fechaActualiza.equals(that.fechaActualiza) : that.fechaActualiza != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (entidadId != null ? entidadId.hashCode() : 0);
        result = 31 * result + (miembroId != null ? miembroId.hashCode() : 0);
        result = 31 * result + (refTipoMiembro != null ? refTipoMiembro.hashCode() : 0);
        result = 31 * result + (estado != null ? estado.hashCode() : 0);
        result = 31 * result + (fechaRegistra != null ? fechaRegistra.hashCode() : 0);
        result = 31 * result + (fechaActualiza != null ? fechaActualiza.hashCode() : 0);
        return result;
    }
}
