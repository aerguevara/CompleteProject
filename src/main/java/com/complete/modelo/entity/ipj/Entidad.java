package com.complete.modelo.entity.ipj;

import com.complete.modelo.entity.catalogo.Catalogo;
import com.complete.modelo.entity.catalogo.DivisionPolitica;
import com.complete.modelo.entity.seguridad.Usuario;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Angelo on 27/09/2016.
 */
@Entity
@Table(name = "entidad", schema = "ipj", catalog = "ipj_desarrollo")
@SequenceGenerator(name = "entidad_id_seq", sequenceName = "ipj.entidad_id_seq", allocationSize = 1)
public class Entidad implements Serializable {

    private Integer id;
    private Entidad entidadId;
    private String descripcion;
    private Catalogo refTipoEntidad;
    private DivisionPolitica refMunicipio;
    private String direccion;
    private String telefono;
    private Usuario usuarioRegistra;
    private Timestamp fechaRegistra;
    private Timestamp fechaActualiza;
    private Usuario usuarioActualiza;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entidad_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "entidad_id",referencedColumnName = "id")
    public Entidad getEntidadId() {
        return entidadId;
    }

    public void setEntidadId(Entidad entidadId) {
        this.entidadId = entidadId;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @ManyToOne
    @JoinColumn(name = "ref_tipo_entidad",referencedColumnName = "codigo")
    public Catalogo getRefTipoEntidad() {
        return refTipoEntidad;
    }

    public void setRefTipoEntidad(Catalogo refTipoEntidad) {
        this.refTipoEntidad = refTipoEntidad;
    }

    @ManyToOne
    @JoinColumn(name = "ref_municipio",referencedColumnName = "codigo")
    public DivisionPolitica getRefMunicipio() {
        return refMunicipio;
    }

    public void setRefMunicipio(DivisionPolitica refMunicipio) {
        this.refMunicipio = refMunicipio;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "fecha_registra")
    public Timestamp getFechaRegistra() {
        return fechaRegistra;
    }

    public void setFechaRegistra(Timestamp fechaRegistra) {
        this.fechaRegistra = fechaRegistra;
    }

    @Basic
    @Column(name = "fecha_actualiza")
    public Timestamp getFechaActualiza() {
        return fechaActualiza;
    }

    public void setFechaActualiza(Timestamp fechaActualiza) {
        this.fechaActualiza = fechaActualiza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entidad entidad = (Entidad) o;

        if (id != null ? !id.equals(entidad.id) : entidad.id != null) return false;
        if (entidadId != null ? !entidadId.equals(entidad.entidadId) : entidad.entidadId != null) return false;
        if (descripcion != null ? !descripcion.equals(entidad.descripcion) : entidad.descripcion != null) return false;
        if (refTipoEntidad != null ? !refTipoEntidad.equals(entidad.refTipoEntidad) : entidad.refTipoEntidad != null)
            return false;
        if (refMunicipio != null ? !refMunicipio.equals(entidad.refMunicipio) : entidad.refMunicipio != null)
            return false;
        if (direccion != null ? !direccion.equals(entidad.direccion) : entidad.direccion != null) return false;
        if (telefono != null ? !telefono.equals(entidad.telefono) : entidad.telefono != null) return false;
        if (fechaRegistra != null ? !fechaRegistra.equals(entidad.fechaRegistra) : entidad.fechaRegistra != null)
            return false;
        if (fechaActualiza != null ? !fechaActualiza.equals(entidad.fechaActualiza) : entidad.fechaActualiza != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (entidadId != null ? entidadId.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (refTipoEntidad != null ? refTipoEntidad.hashCode() : 0);
        result = 31 * result + (refMunicipio != null ? refMunicipio.hashCode() : 0);
        result = 31 * result + (direccion != null ? direccion.hashCode() : 0);
        result = 31 * result + (telefono != null ? telefono.hashCode() : 0);
        result = 31 * result + (fechaRegistra != null ? fechaRegistra.hashCode() : 0);
        result = 31 * result + (fechaActualiza != null ? fechaActualiza.hashCode() : 0);
        return result;
    }
}
