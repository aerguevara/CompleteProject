package com.complete.modelo.entity.ipj;


import com.complete.modelo.entity.catalogo.Catalogo;
import com.complete.modelo.entity.seguridad.Usuario;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Angelo on 21/05/2016.
 */
@Entity
@Table(name = "miembro", schema = "ipj", catalog = "ipj_desarrollo")
@SequenceGenerator(name = "miembro_id_seq", sequenceName = "ipj.miembro_id_seq", allocationSize = 1)
public class Miembro implements Serializable {
    private Integer id;
    private String nombreCompleto;
    private Catalogo estadoCivil;
    private Usuario usuarioRegistra;
    private Usuario usuarioActualiza;
    private Timestamp fechaRegistra;
    private Timestamp fechaModifica;
    private Catalogo sexo;
    private Catalogo refRed;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "miembro_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_completo")
    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Miembro miembro = (Miembro) o;

        if (id != null ? !id.equals(miembro.id) : miembro.id != null) return false;
        if (nombreCompleto != null ? !nombreCompleto.equals(miembro.nombreCompleto) : miembro.nombreCompleto != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombreCompleto != null ? nombreCompleto.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "ref_estado_civil",referencedColumnName = "codigo")
    public Catalogo getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(Catalogo estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @ManyToOne
    @JoinColumn(name = "usuario_registra",referencedColumnName = "id")
    public Usuario getUsuarioRegistra() {
        return usuarioRegistra;
    }

    public void setUsuarioRegistra(Usuario usuarioRegistra) {
        this.usuarioRegistra = usuarioRegistra;
    }
    @ManyToOne
    @JoinColumn(name = "usuario_actualiza",referencedColumnName = "id")
    public Usuario getUsuarioActualiza() {
        return usuarioActualiza;
    }

    public void setUsuarioActualiza(Usuario usuarioActualiza) {
        this.usuarioActualiza = usuarioActualiza;
    }
    @Basic
    @Column(name = "fecha_registra")
    public Timestamp getFechaRegistra() {
        return fechaRegistra;
    }

    public void setFechaRegistra(Timestamp fechaRegistra) {
        this.fechaRegistra = fechaRegistra;
    }
    @Basic
    @Column(name = "fecha_actualiza")
    public Timestamp getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Timestamp fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    @ManyToOne
    @JoinColumn(name = "ref_sexo",referencedColumnName = "codigo")
    public Catalogo getSexo() {
        return sexo;
    }

    public void setSexo(Catalogo sexo) {
        this.sexo = sexo;
    }

    @ManyToOne
    @JoinColumn(name = "ref_red",referencedColumnName = "codigo")
    public Catalogo getRefRed() {
        return refRed;
    }

    public void setRefRed(Catalogo refRed) {
        this.refRed = refRed;
    }
}
