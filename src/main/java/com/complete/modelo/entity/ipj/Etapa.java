package com.complete.modelo.entity.ipj;



import com.complete.modelo.entity.catalogo.Catalogo;
import com.complete.modelo.entity.seguridad.Usuario;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Angelo on 21/05/2016.
 */
@Entity
@Table(name = "etapa", schema = "ipj", catalog = "ipj_desarrollo")
public class Etapa implements Serializable {
    private Integer id;
    private Timestamp fecha;
    private Catalogo etapaEstudio;
    private Miembro miembro;
    private Usuario usuarioRegistra;
    private Usuario usuarioActualiza;
    private Timestamp fechaRegistra;
    private Timestamp fechaModifica;



    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha")
    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Etapa etapa = (Etapa) o;

        if (fecha != null ? !fecha.equals(etapa.fecha) : etapa.fecha != null) return false;
        if (id != null ? !id.equals(etapa.id) : etapa.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (fecha != null ? fecha.hashCode() : 0);
        return result;
    }
    @ManyToOne
    @JoinColumn(name = "ref_etapa",referencedColumnName = "codigo")
    public Catalogo getEtapaEstudio() {
        return etapaEstudio;
    }

    public void setEtapaEstudio(Catalogo etapaEstudio) {
        this.etapaEstudio = etapaEstudio;
    }
    @ManyToOne
    @JoinColumn(name = "id_miembro",referencedColumnName = "id")
    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    @ManyToOne
    @JoinColumn(name = "usuario_registra",referencedColumnName = "id")
    public Usuario getUsuarioRegistra() {
        return usuarioRegistra;
    }

    public void setUsuarioRegistra(Usuario usuarioRegistra) {
        this.usuarioRegistra = usuarioRegistra;
    }
    @ManyToOne
    @JoinColumn(name = "usuario_actualiza",referencedColumnName = "id")
    public Usuario getUsuarioActualiza() {
        return usuarioActualiza;
    }

    public void setUsuarioActualiza(Usuario usuarioActualiza) {
        this.usuarioActualiza = usuarioActualiza;
    }
    @Basic
    @Column(name = "fecha_registra")
    public Timestamp getFechaRegistra() {
        return fechaRegistra;
    }

    public void setFechaRegistra(Timestamp fechaRegistra) {
        this.fechaRegistra = fechaRegistra;
    }
    @Basic
    @Column(name = "fecha_actualiza")
    public Timestamp getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Timestamp fechaModifica) {
        this.fechaModifica = fechaModifica;
    }
}
