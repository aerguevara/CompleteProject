package com.complete.modelo.entity.seguridad;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Angelo on 29/05/2016.
 */
@Entity
@Table(name = "rol", schema = "seguridad", catalog = "ipj_desarrollo")
@SequenceGenerator(name = "rol_id_seq", sequenceName = "seguridad.rol_id_seq", allocationSize = 1)
public class Rol implements Serializable {
    private Integer id;
    private String nombre;
    private String descripcion;
    private Boolean estado;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rol_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "estado")
    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rol rol = (Rol) o;

        if (descripcion != null ? !descripcion.equals(rol.descripcion) : rol.descripcion != null) return false;
        if (estado != null ? !estado.equals(rol.estado) : rol.estado != null) return false;
        if (id != null ? !id.equals(rol.id) : rol.id != null) return false;
        if (nombre != null ? !nombre.equals(rol.nombre) : rol.nombre != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (estado != null ? estado.hashCode() : 0);
        return result;
    }
}
