package com.complete.modelo.entity.seguridad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Angelo on 29/05/2016.
 */
@Entity
@Table(name = "usuariorol", schema = "seguridad", catalog = "ipj_desarrollo")
public class Usuariorol implements Serializable {
    private Integer idUsuario;
    private Integer idRol;
    @Id
    @Column(name = "id_usuario")
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Id
    @Column(name = "id_rol")
    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Usuariorol that = (Usuariorol) o;

        if (idRol != null ? !idRol.equals(that.idRol) : that.idRol != null) return false;
        if (idUsuario != null ? !idUsuario.equals(that.idUsuario) : that.idUsuario != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idUsuario != null ? idUsuario.hashCode() : 0;
        result = 31 * result + (idRol != null ? idRol.hashCode() : 0);
        return result;
    }
}
