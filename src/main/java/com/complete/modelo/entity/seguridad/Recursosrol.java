package com.complete.modelo.entity.seguridad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Angelo on 29/05/2016.
 */
@Entity
@Table(name = "recursosrol", schema = "seguridad", catalog = "ipj_desarrollo")
public class Recursosrol implements Serializable {
    private Integer idRecurso;
    private Integer idRol;

    @Id
    @Column(name = "id_recurso")
    public Integer getIdRecurso() {
        return idRecurso;
    }

    public void setIdRecurso(Integer idRecurso) {
        this.idRecurso = idRecurso;
    }

    @Id
    @Column(name = "id_rol")
    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recursosrol that = (Recursosrol) o;

        if (idRecurso != null ? !idRecurso.equals(that.idRecurso) : that.idRecurso != null) return false;
        if (idRol != null ? !idRol.equals(that.idRol) : that.idRol != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRecurso != null ? idRecurso.hashCode() : 0;
        result = 31 * result + (idRol != null ? idRol.hashCode() : 0);
        return result;
    }
}
