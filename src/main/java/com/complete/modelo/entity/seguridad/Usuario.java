package com.complete.modelo.entity.seguridad;

import com.complete.modelo.entity.ipj.Entidad;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Angelo on 29/05/2016.
 */
@Entity
@Table(name = "usuario", schema = "seguridad", catalog = "ipj_desarrollo")
@SequenceGenerator(name = "usuario_id_seq", sequenceName = "seguridad.usuario_id_seq", allocationSize = 1)
public class Usuario implements Serializable {
    private Integer id;
    private String nombre;
    private String contrasenia;
    private Boolean estado;
    private String nombreCompleto;
    private Entidad entidadId;

    @ManyToOne
    @JoinColumn(name = "entidad_id",referencedColumnName = "id")
    public Entidad getEntidadId() {
        return entidadId;
    }

    public void setEntidadId(Entidad entidadId) {
        this.entidadId = entidadId;
    }

    @Basic
    @Column(name = "nombre_completo")
    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "contrasenia")
    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    @Basic
    @Column(name = "estado")
    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Usuario usuario = (Usuario) o;

        if (contrasenia != null ? !contrasenia.equals(usuario.contrasenia) : usuario.contrasenia != null) return false;
        if (estado != null ? !estado.equals(usuario.estado) : usuario.estado != null) return false;
        if (id != null ? !id.equals(usuario.id) : usuario.id != null) return false;
        if (nombre != null ? !nombre.equals(usuario.nombre) : usuario.nombre != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (contrasenia != null ? contrasenia.hashCode() : 0);
        result = 31 * result + (estado != null ? estado.hashCode() : 0);
        return result;
    }
}
