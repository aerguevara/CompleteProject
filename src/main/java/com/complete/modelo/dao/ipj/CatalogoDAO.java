package com.complete.modelo.dao.ipj;

import com.complete.logic.service.ipj.CatalogoService;
import com.complete.modelo.dao.BaseGenericDAO;
import com.complete.modelo.entity.catalogo.Catalogo;

/**
 * Created by Angelo on 26/09/2016.
 */
public interface CatalogoDAO extends BaseGenericDAO<Catalogo,Integer> {
}
