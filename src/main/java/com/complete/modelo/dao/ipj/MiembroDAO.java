package com.complete.modelo.dao.ipj;

import com.complete.modelo.dao.BaseGenericDAO;
import com.complete.modelo.entity.ipj.Miembro;

/**
 * Created by Angelo on 24/09/2016.
 */
public interface MiembroDAO extends BaseGenericDAO<Miembro, Integer> {
}
