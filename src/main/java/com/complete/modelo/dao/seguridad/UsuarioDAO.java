package com.complete.modelo.dao.seguridad;

import com.complete.modelo.dao.BaseGenericDAO;
import com.complete.modelo.entity.seguridad.Usuario;

/**
 * Created by Angelo on 04/09/2016.
 */

public  interface UsuarioDAO extends BaseGenericDAO<Usuario, Integer> {


}
