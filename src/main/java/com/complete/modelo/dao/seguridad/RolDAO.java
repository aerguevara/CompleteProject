package com.complete.modelo.dao.seguridad;

import com.complete.modelo.dao.BaseGenericDAO;
import com.complete.modelo.entity.seguridad.Rol;

import java.util.List;

/**
 * Created by Angelo on 15/09/2016.
 */
public interface RolDAO extends BaseGenericDAO<Rol, Integer> {

    List<Rol> buscarRolPorUsuarioId(int id);
}
