package com.complete.modelo.dao;

/**
 * Created by Angelo on 31/08/2016.
 */
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public abstract interface BaseGenericDAO<T, ID extends Serializable> {
    public abstract T find(ID paramID)
            throws EntityNotFoundException;

    public abstract <T> T[] find(Serializable... paramVarArgs);
    public abstract T save(Object paramObject) throws DAOException;
    public abstract void save(Object... paramVarArgs) throws DAOException;
    public abstract T saveUpper(Object paramObject) throws DAOException;
    public abstract void saveUpper(Object... paramVarArgs) throws DAOException;
    public abstract void updateUpper(Object paramObject) throws DAOException;
    public abstract void update(Object paramObject) throws DAOException;
    public abstract boolean remove(Object paramObject);
    public abstract void remove(Object... paramVarArgs);
    public abstract boolean removeId(Serializable paramSerializable);
    public abstract void removeId(Serializable... paramVarArgs);
    public abstract List<T> findAll();
    public abstract boolean isAttached(Object paramObject);
    public abstract void refresh(Object... paramVarArgs);
    public abstract void flush();
    public abstract List search(ISearch paramISearch);
    public abstract int count(ISearch paramISearch);
    public abstract int count();
    public abstract SearchResult searchAndCount(ISearch paramISearch);
    public abstract SearchResult lazySearch(int paramInt1, int paramInt2);
    public abstract SearchResult lazySearch(int paramInt1, int paramInt2, String paramString1, String paramString2, Map<String, Object> paramMap);
    public abstract Object searchUnique(ISearch paramISearch);
}
