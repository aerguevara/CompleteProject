package com.complete.modelo.daoImpl.seguridad;

import com.complete.modelo.dao.seguridad.RolDAO;
import com.complete.modelo.daoImpl.BaseGenericDAOImpl;
import com.complete.modelo.entity.seguridad.Rol;
import org.hibernate.Query;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Angelo on 15/09/2016.
 */
@Repository
public class RolDAOImpl extends BaseGenericDAOImpl<Rol,Integer> implements RolDAO {


    @Override
    public List<Rol> buscarRolPorUsuarioId(int id) {
        Query query = this.sessionFactory.getCurrentSession().
                createSQLQuery("select b.id,b.nombre,b.descripcion,b.estado from seguridad.usuariorol a \n" +
                                "inner join seguridad.rol b \n" +
                                "on b.id = a.id_rol \n" +
                                "where a.id_usuario = :id");

        query.setInteger("id", id);

        query.setResultTransformer(new AliasToBeanResultTransformer(Rol.class));

        return query.list();
    }
}
