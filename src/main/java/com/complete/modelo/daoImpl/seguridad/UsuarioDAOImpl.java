package com.complete.modelo.daoImpl.seguridad;

import com.complete.modelo.dao.seguridad.UsuarioDAO;
import com.complete.modelo.daoImpl.BaseGenericDAOImpl;
import com.complete.modelo.entity.seguridad.Usuario;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Angelo on 04/09/2016.
 */
@Repository
public class UsuarioDAOImpl extends BaseGenericDAOImpl<Usuario,Integer> implements UsuarioDAO {


}
