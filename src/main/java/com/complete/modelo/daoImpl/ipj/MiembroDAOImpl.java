package com.complete.modelo.daoImpl.ipj;

import com.complete.modelo.dao.ipj.MiembroDAO;
import com.complete.modelo.daoImpl.BaseGenericDAOImpl;
import com.complete.modelo.entity.ipj.Miembro;
import org.springframework.stereotype.Repository;

/**
 * Created by Angelo on 24/09/2016.
 */
@Repository
public class MiembroDAOImpl extends BaseGenericDAOImpl<Miembro,Integer> implements MiembroDAO {
}
