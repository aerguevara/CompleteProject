package com.complete.modelo.daoImpl.ipj;

import com.complete.modelo.dao.ipj.DivisionPoliticaDAO;
import com.complete.modelo.daoImpl.BaseGenericDAOImpl;
import com.complete.modelo.entity.catalogo.DivisionPolitica;
import org.springframework.stereotype.Repository;

/**
 * Created by Angelo on 24/09/2016.
 */
@Repository
public class DivisionPoliticaDAOImpl  extends BaseGenericDAOImpl<DivisionPolitica,Integer> implements DivisionPoliticaDAO {
}
