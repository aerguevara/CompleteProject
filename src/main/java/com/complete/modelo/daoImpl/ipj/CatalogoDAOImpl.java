package com.complete.modelo.daoImpl.ipj;

import com.complete.modelo.dao.ipj.CatalogoDAO;
import com.complete.modelo.daoImpl.BaseGenericDAOImpl;
import com.complete.modelo.entity.catalogo.Catalogo;
import org.springframework.stereotype.Repository;

/**
 * Created by Angelo on 26/09/2016.
 */
@Repository
public class CatalogoDAOImpl extends BaseGenericDAOImpl<Catalogo,Integer>  implements CatalogoDAO{
}
