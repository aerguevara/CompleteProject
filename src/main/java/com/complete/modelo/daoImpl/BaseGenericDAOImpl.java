
package com.complete.modelo.daoImpl;
/**
 * Created by Angelo on 31/08/2016.
 */
import com.complete.modelo.dao.BaseGenericDAO;
import com.complete.modelo.dao.DAOException;
import com.complete.modelo.dao.EntityNotFoundException;
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;
import com.googlecode.genericdao.search.hibernate.HibernateMetadataUtil;
import com.googlecode.genericdao.search.hibernate.HibernateSearchProcessor;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseGenericDAOImpl<T, ID extends Serializable> implements BaseGenericDAO<T, ID> {

    Logger logger = Logger.getLogger(BaseGenericDAOImpl.class);

    @Autowired
    public SessionFactory sessionFactory;
    private Class<T> entityClass;
    private HibernateSearchProcessor searchProcessor;
    private HibernateMetadataUtil metadataUtil;

    public BaseGenericDAOImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = ((Class) genericSuperclass.getActualTypeArguments()[0]);
    }

    @PostConstruct
    public void init() {
        this.searchProcessor = HibernateSearchProcessor.getInstanceForSessionFactory(this.sessionFactory);
        this.metadataUtil = HibernateMetadataUtil.getInstanceForSessionFactory(this.sessionFactory);

    }
    public T find(Serializable id) throws EntityNotFoundException {
        T entity = (T) getSession().get(this.entityClass, id);
        if (entity == null) {
            throw new EntityNotFoundException(((Integer) id).intValue());
        }
        return entity;
    }

    public <T> T[] find(Serializable... ids) {
        Criteria c = getSession().createCriteria(this.entityClass);
        c.add(Restrictions.in("id", ids));
        Object[] retVal = (Object[]) Array.newInstance(this.entityClass, ids.length);
        for (Object entity : c.list()) {
            int i = 0;
            if (i < ids.length) {
                retVal[i] = entity;
            }
        }
        return (T[]) retVal;

    }

    public T save(Object entity) throws DAOException {
        try {
            toTrim(entity);
            return (T) getSession().save(entity);
        } catch ( InvocationTargetException e) {
            this.logger.error(e);
            throw new DAOException(e);
        } catch (NoSuchMethodException e) {
            this.logger.error(e);
            throw new DAOException(e);
        } catch (IllegalAccessException e) {
            this.logger.error(e);
            throw new DAOException(e);
        }

    }

    public void save(Object... entities) throws DAOException {
        for (Object entity : entities) {
            if (entity != null) {
                save(entity);
            }
        }
    }

    public T saveUpper(Object entity) throws DAOException {
        try {
            toUpperCaseTrim(entity);
            return (T) getSession().save(entity);
        }
        catch ( InvocationTargetException e) {
            this.logger.error(e);
            throw new DAOException(e);
        } catch (NoSuchMethodException e) {
            this.logger.error(e);
            throw new DAOException(e);
        } catch (IllegalAccessException e) {
            this.logger.error(e);
            throw new DAOException(e);
        }

    }

    public void saveUpper(Object... entities) throws DAOException {
        for (Object entity : entities) {
            if (entity != null) {
                saveUpper(entity);
            }
        }
    }

    public void updateUpper(Object entity) throws DAOException {
        try {
            toUpperCaseTrim(entity);
            getSession().merge(entity);
        }
        catch (IllegalAccessException e) {
            this.logger.error(e);
            throw new DAOException(e);
        } catch (NoSuchMethodException e) {
            this.logger.error(e);
            throw new DAOException(e);
        } catch (InvocationTargetException e) {
            this.logger.error(e);
            throw new DAOException(e);
        }

    }

    public void update(Object entity) throws DAOException {
        try {
            toTrim(entity);
            getSession().merge(entity);
        } catch ( InvocationTargetException e) {
            this.logger.error(e);
            throw new DAOException(e);
        } catch (NoSuchMethodException e) {
            this.logger.error(e);
            throw new DAOException(e);
        } catch (IllegalAccessException e) {
            this.logger.error(e);
            throw new DAOException(e);
        }

    }

    public boolean remove(Object entity) {
        if (entity != null) {
            getSession().delete(entity);
            return true;
        }
        return false;

    }

    public void remove(Object... entities) {
        for (Object entity : entities) {
            if (entity != null) {
                remove(entity);
            }
        }
    }

    public boolean removeId(Serializable id) {
        if (id != null) {
            Object entity = getSession().get(this.entityClass, id);
            if (entity != null) {
                remove(entity);
                return true;
            }
        }
        return false;
    }

    public void removeId(Serializable... ids) {
        Criteria c = getSession().createCriteria(this.entityClass);
        c.add(Restrictions.in("id", ids));
        for (Object entity : c.list()) {
            if (entity != null) {
                remove(entity);
            }
        }
    }

    public List<T> findAll() {
        return getSession().createCriteria(this.entityClass).list();
    }

    public boolean isAttached(Object entity) {
        return getSession().contains(entity);
    }

    public void refresh(Object... entities) {
        for (Object entity : entities) {
            if (entity != null) {
                getSession().refresh(entities);
            }
        }
    }

    public void flush() {
        getSession().flush();
    }

    public List search(ISearch search) {
        if (search == null)
            throw new NullPointerException("Search is null.");
        if ((search.getSearchClass() != null) && (!search.getSearchClass().equals(this.entityClass))) {
            throw new IllegalArgumentException("Search class does not match expected type: " + this.entityClass.getName());
        }
        return this.searchProcessor.search(getSession(), this.entityClass, search);
    }

    public int count(ISearch search) {
        if (search == null)
            throw new NullPointerException("Search is null.");
        if ((search.getSearchClass() != null) && (!search.getSearchClass().equals(this.entityClass))) {
            throw new IllegalArgumentException("Search class does not match expected type: " + this.entityClass.getName());
        }
        return this.searchProcessor.count(getSession(), this.entityClass, search);
    }

    public int count() {
        List counts = getSession().createQuery("select count(_it_) from " + this.metadataUtil.get(this.entityClass).getEntityName() + " _it_").list();
        int sum = 0;
        for (Object count : counts) {
            sum += ((Long) count).intValue();
        }
        return sum;
    }

    public SearchResult searchAndCount(ISearch search) {
        if (search == null)
            throw new NullPointerException("Search is null.");
        if ((search.getSearchClass() != null) && (!search.getSearchClass().equals(this.entityClass))) {
            throw new IllegalArgumentException("Search class does not match expected type: " + this.entityClass.getName());
        }
        return this.searchProcessor.searchAndCount(getSession(), this.entityClass, search);

    }

    public SearchResult lazySearch(int first, int pageSize) {
        List<T> list = null;
        int totalCount = 0;
        SearchResult searchResult = new SearchResult();
        Search search = new Search(this.entityClass);
        totalCount = this.searchProcessor.count(getSession(), search);
        searchResult.setTotalCount(totalCount);
        if (totalCount > 0) {
            search.setFirstResult(first);
            search.setMaxResults(pageSize);
            list = this.searchProcessor.search(getSession(), search);
        }
        if (list == null) {
            list = new ArrayList();
        }
        searchResult.setResult(list);
        return searchResult;
    }

    public SearchResult lazySearch(int first, int pageSize, String sortField, String sortOrder, Map<String, Object> filters) {
        List<T> list = null;
        int totalCount = 0;
        SearchResult searchResult = new SearchResult();
        Search search = new Search(this.entityClass);
        Iterator<String> it;
        if ((filters != null) && (!filters.isEmpty())) {
            for (it = filters.keySet().iterator(); it.hasNext(); ) {
                String filterProperty = (String) it.next();
                Object filterValue = filters.get(filterProperty);
                if ((filterValue instanceof String)) {
                    search.addFilterILike(filterProperty, "%" + filterValue.toString() + "%");
                }
                else if ((filterValue instanceof String[])) {
                String[] arrayFilterValue = (String[]) filterValue;
                List<String> listFilterValue = Arrays.asList(arrayFilterValue);
                search.addFilterIn(filterProperty, listFilterValue);
            } else {
                search.addFilterEqual(filterProperty, filterValue);
            }
            }
        }
        totalCount = this.searchProcessor.count(getSession(), search);
        searchResult.setTotalCount(totalCount);
        if (totalCount > 0) {
            if ((!StringUtils.isBlank(sortField)) && (!StringUtils.isBlank(sortOrder))) {
                if (sortOrder.toLowerCase().contains("asc")) {
                    search.addSortAsc(sortField);
                }
            }
            search.setFirstResult(first);
            search.setMaxResults(pageSize);
            list = this.searchProcessor.search(getSession(), search);
        }
        if (list == null) {
            list = new ArrayList();
        }
        searchResult.setResult(list);
        return searchResult;
    }

    public Object searchUnique(ISearch search) {
        if (search == null)
            throw new NullPointerException("Search is null.");
        if ((search.getSearchClass() != null) && (!search.getSearchClass().equals(this.entityClass))) {
            throw new IllegalArgumentException("Search class does not match expected type: " + this.entityClass.getName());
        }
        return this.searchProcessor.searchUnique(getSession(), this.entityClass, search);
    }

    private void toUpperCaseTrim(Object entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Field[] fields = entity.getClass().getDeclaredFields();
        String methodList = getMethodsNameByEntity(entity);
        if (methodList == null) {
            return;
        }
        for (Field f : fields) {
            String field = String.valueOf(f.getName().charAt(0)).toUpperCase() + f.getName().substring(1);
            if ((f.getType() == String.class) && (methodList.contains("get" + field)) && (methodList.contains("set" + field))) {
                Method getter = entity.getClass().getDeclaredMethod("get" + field, new Class[0]);
                Method setter = entity.getClass().getDeclaredMethod("set" + field, new Class[]{String.class});
                Object value = getter.invoke(entity, new Object[0]);
                if (value != null) {
                    setter.invoke(entity, new Object[]{((String) value).trim().toUpperCase()});
                }
            }
        }
    }

    private void toTrim(Object entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Field[] fields = entity.getClass().getDeclaredFields();
        String methodList = getMethodsNameByEntity(entity);
        if (methodList.isEmpty()) {
            return;
        }
        for (Field f : fields) {
            String field = String.valueOf(f.getName().charAt(0)).toUpperCase() + f.getName().substring(1);
            if ((f.getType() == String.class) && (methodList.contains("get" + field)) && (methodList.contains("set" + field))) {
                Method getter = entity.getClass().getDeclaredMethod("get" + field, new Class[0]);
                Method setter = entity.getClass().getDeclaredMethod("set" + field, new Class[]{String.class});
                Object value = getter.invoke(entity, new Object[0]);
                if (value != null) {
                    setter.invoke(entity, new Object[]{((String) value).trim()});
                }
            }
        }
    }

    private Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    private String getMethodsNameByEntity(Object entity) {
        String methodList = "";
        Method[] methods = entity.getClass().getDeclaredMethods();
        if (methods != null) {
            methodList = Arrays.toString(methods);
        }
        return methodList;
    }
}
