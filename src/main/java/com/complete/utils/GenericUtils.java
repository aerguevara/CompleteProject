package com.complete.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
/**
 * Created by Angelo on 09/09/2016.
 */

public class GenericUtils {
    private static Logger logger = Logger.getLogger(GenericUtils.class);

    public static String getRemoteIp() {
        String strRemoteIp = "";
        try {
            InetAddress ip = InetAddress.getLocalHost();
            strRemoteIp = ip.getHostAddress();
        } catch (UnknownHostException e) {
            logger.error(e);
        }
        return strRemoteIp;
    }

    public static Date now() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();

    }

    public static Timestamp nowTimeStamp() {
        return new Timestamp(now().getTime());
    }

}

