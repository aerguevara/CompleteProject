package com.complete.utils;

/**
 * Created by Angelo on 04/09/2016.
 */

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class SeguridadUtils {

    public static boolean verificarContrasenia(String texto1, String texto2){
        boolean result = BCrypt.checkpw(texto1, texto2);
        return result;

    }

    public static String encriptar(String texto){

        String textoencriptado;
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        textoencriptado = passwordEncoder.encode(texto);
        return textoencriptado;

    }
}
