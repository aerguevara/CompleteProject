package com.complete.utils;

import com.complete.logic.service.seguridad.RolService;
import com.complete.modelo.entity.seguridad.Rol;
import com.complete.modelo.entity.seguridad.Usuario;
import com.complete.vista.loginBackBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Angelo on 15/09/2016.
 */
public class BaseBean {

    private static final Logger LOGGER = Logger.getLogger(BaseBean.class);

    @Autowired
    @Qualifier("loginBackBean")
    protected loginBackBean login;
    private String rolUsuarioActual;
    private Usuario usuarioActual;


    @PostConstruct
    public void initBase(){
        //cargo la variable del nombre del usuario actual
        usuarioActual = login.getUserSesionActual();
        //cargo el rol de usuario
        rolUsuarioActual = login.getRolUsuarioActual();
    }

    public Usuario getUsuarioActual() {

        return usuarioActual;
    }

    public void setUsuarioActual(Usuario usuarioActual) {
        this.usuarioActual = usuarioActual;
    }

    public String getRolUsuarioActual() {
        return rolUsuarioActual;
    }

    public void setRolUsuarioActual(String rolUsuarioActual) {
        this.rolUsuarioActual = rolUsuarioActual;
    }
}
