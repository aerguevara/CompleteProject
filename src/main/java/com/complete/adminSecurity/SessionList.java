
package com.complete.adminSecurity;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class SessionList implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(SessionList.class);

    private static final long serialVersionUID = 1L;
    private static Map<String, HttpSession> userList = new HashMap();

    public static void addUser(String userName, HttpSession session) {
        userList.put(userName, session);
        LOGGER.info("******************************Agregando usuario con sesion iniciada: " + userName);
    }

    public static void remove(String userName) {
        if (contains(userName)) {
            userList.remove(userName);
            LOGGER.info("***************************eliminado usuario con sesion iniciada: " + userName);
        }
    }

    public static boolean contains(String userName) {
        return userList.containsKey(userName);
    }

    public static HttpSession getSession(String userName) {
        return (HttpSession) userList.get(userName);
    }

    public static String getSessionId(String userName) {
        return ((HttpSession) userList.get(userName)).getId();
    }

    public static Set<String> getUserList() {
        return userList.keySet();
    }

}
