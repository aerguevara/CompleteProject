
package com.complete.adminSecurity;


import com.complete.vista.loginBackBean;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@WebListener
public class SessionListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
    }

    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

        loginBackBean oSessionBean = (loginBackBean) httpSessionEvent.getSession().getAttribute("loginBackBean");

        if ((oSessionBean != null) && (oSessionBean.isLogin())) {

           SessionList.remove(oSessionBean.getUserSesionActual().getNombre());

        }

    }

}