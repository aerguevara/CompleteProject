
package com.complete.adminSecurity;

import com.complete.vista.loginBackBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class SecurityFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        String contextPath = ((HttpServletRequest) req).getContextPath();
        String requestUri = ((HttpServletRequest) req).getRequestURI();
        HttpServletRequest elementoRequest = (HttpServletRequest) req;

        if (getLoggedIn((HttpServletRequest) req)) {

            loginBackBean oSessionBean = obtenerSession((HttpServletRequest) req);
            String userName = oSessionBean.getUserSesionActual().getNombre();
            HttpSession httpSession = ((HttpServletRequest) req).getSession();
            String sessionId = httpSession.getId();
            //obtengo si la accion que disparo el filter no fue ajax para limpiar la pagina actual
            boolean isAjax = "XMLHttpRequest".equals(elementoRequest.getHeader("X-Requested-With"));
            if(!isAjax){
                oSessionBean.setPaginaActual("");
            }


            if ((SessionList.contains(userName)) && (SessionList.getSessionId(userName) != sessionId)) {
                ((HttpServletRequest) req).getRequestDispatcher("/login.xhtml?faces-redirect=true").forward(req, res);
            } else {
                if (!SessionList.contains(userName)) {
                    SessionList.addUser(userName, ((HttpServletRequest) req).getSession());
                    ((HttpServletRequest) req).getRequestDispatcher("/index.xhtml?faces-redirect=true").forward(req, res);
                } else {
                    ((HttpServletRequest) req).getRequestDispatcher("/index.xhtml?faces-redirect=true").forward(req, res);
                }
            }
        }else{
            ((HttpServletRequest) req).getRequestDispatcher("/login.xhtml?faces-redirect=true").forward(req, res);
        }
    }



    private boolean getLoggedIn(HttpServletRequest req) {
        loginBackBean oSessionBean = (loginBackBean) req.getSession().getAttribute("loginBackBean");
        return (oSessionBean != null) && (oSessionBean.isLogin());

    }

    private loginBackBean obtenerSession(HttpServletRequest req) {
        return (loginBackBean) req.getSession().getAttribute("loginBackBean");
    }

}


