package com.complete.vista;

import com.complete.utils.BaseBean;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Angelo on 16/09/2016.
 */
@Named
@Scope("view")
public class celulaBackBean extends BaseBean implements Serializable {

    private String user;

    @PostConstruct
    private void init(){
       user = this.getUsuarioActual().getNombre();
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
