package com.complete.vista;

import com.complete.logic.service.seguridad.RolService;
import com.complete.logic.service.seguridad.UsuarioService;
import com.complete.modelo.entity.seguridad.Rol;
import com.complete.modelo.entity.seguridad.Usuario;
import com.complete.utils.FacesUtils;
import com.complete.utils.SeguridadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.servlet.http.HttpServletRequest;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Angelo on 04/09/2016.
 */
@Named
@Scope("session")
public class loginBackBean implements Serializable{

    private static final long serialVersionUID = 1L;

    private String username;
    private String contrasenia;
    private boolean login;
    private Usuario userSesionActual;
    private String paginaActual;
    private String rolUsuarioActual;

    @Autowired
    UsuarioService oUsuarioService;
    @Autowired
    RolService oRolService;


    public String iniciarSesion(){

        this.userSesionActual = oUsuarioService.BuscarUsuario(username.toUpperCase());

        if(userSesionActual != null && SeguridadUtils.verificarContrasenia(contrasenia,userSesionActual.getContrasenia())){
            if(!userSesionActual.getEstado()){
                FacesUtils.addErrorMessage("Usuario inactivo");
            }else{
                this.login = true;
                cargarRolUsuarioActual();
                HttpServletRequest oRequest =  FacesUtils.getServletRequest();
                oRequest.getSession().setAttribute("usuarioActual",userSesionActual);
                return "index?faces-redirect=true";
            }
        }else{
            FacesUtils.addErrorMessage("Usuario o contraseña incorrecta");
        }
        return "";
    }

    public String cerrarSesion(){

        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        ViewHandler handler = context.getApplication().getViewHandler();
        UIViewRoot root = handler.createView(context, viewId);
        root.setViewId(viewId);
        context.setViewRoot(root);
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        return "/login?faces-redirect=true";
    }

    public void cargarRolUsuarioActual(){
        List<Rol> listaRolUsuario = oRolService.buscarRolPorUsuarioId(this.userSesionActual.getId());
        if(listaRolUsuario.size()>0){
            rolUsuarioActual = listaRolUsuario.get(0).getNombre();
        }
    }

    public String getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(String paginaActual) {
        this.paginaActual = paginaActual;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public Usuario getUserSesionActual() {
        return userSesionActual;
    }

    public void setUserSesionActual(Usuario userSesionActual) {
        this.userSesionActual = userSesionActual;
    }

    public String getRolUsuarioActual() {
        return rolUsuarioActual;
    }

    public void setRolUsuarioActual(String rolUsuarioActual) {
        this.rolUsuarioActual = rolUsuarioActual;
    }
}
