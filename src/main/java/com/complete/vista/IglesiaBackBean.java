package com.complete.vista;

import com.complete.logic.service.ipj.CatalogoService;
import com.complete.logic.service.ipj.MiembroService;
import com.complete.modelo.entity.catalogo.Catalogo;
import com.complete.modelo.entity.ipj.Entidad;
import com.complete.modelo.entity.ipj.Miembro;
import org.apache.log4j.Logger;
import com.complete.logic.service.ipj.DivisionPoliticaService;
import com.complete.modelo.dao.DAOException;
import com.complete.modelo.entity.catalogo.DivisionPolitica;
import com.complete.utils.BaseBean;
import com.complete.utils.FacesUtils;
import com.complete.utils.GenericUtils;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


/**
 * Created by Angelo on 15/09/2016.
 */
@Named
@Scope("view")
public class IglesiaBackBean extends BaseBean implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(BaseBean.class);

    private String usuario;
    private List<Entidad> listaEntidad;
    private Entidad EntidadSelected;
    private String prBuscar;
    private boolean desabilitarBotones;
    private boolean esNuevo;
    private String txtNombre;
    private String txtDireccion;
    private int refDepartamentoSelect;
    private int refMunicipioSelect;
    private List<DivisionPolitica> listaDepartamento;
    private List<DivisionPolitica> listaMunicipio;
    private List<Miembro> listaMiembrosPastor;
    private List<Entidad> listaEntidadsHijas;
    private List<Entidad> listaEntidadsAsociar;
    private int EntidadPadreSelect;
    private int miembroPastorSelect;

    //variables para crear al pastor
    private String txtNombreCompleto;
    private String cmbsexo;
    private String cmbEstadoCivil;
    private Catalogo tipoMiembro;
    private List<Catalogo> listaSexo;
    private List<Catalogo> listaEstadoCivil;
    private boolean miembroCreado;





//    @Autowired
//    EntidadService EntidadService;
    @Autowired
    DivisionPoliticaService divisisionPoliticaService;
    @Autowired
    MiembroService miembroService;
    @Autowired
    CatalogoService catalogoService;



    @PostConstruct
    private void init() {
        this.usuario = this.getUsuarioActual().getNombre();
        desabilitarBotones = true;
    }


    public void buscarEntidadPorNombre() {
      //  listaEntidad = EntidadService.BuscarEntidad(prBuscar);
    }

    public void editarOEliminar() {
        if (EntidadSelected != null) {
            desabilitarBotones = false;
        }
    }

    public void eliminar() {
        try {
            if (EntidadSelected != null) {
               // EntidadService.eliminar(EntidadSelected);
                FacesUtils.addInfoMessage("El Registro se elimino Exitosamente");
                EntidadSelected = null;
                desabilitarBotones = true;
                buscarEntidadPorNombre();
            }
        } catch (Exception e) {
            FacesUtils.addErrorMessage("No se pudo eliminar el registro" + e.getMessage());
            e.printStackTrace();
        }


    }

    public void abrirDialogoModificar() {
        if (EntidadSelected != null) {
//            cargarDepartamento();
//            cargarListaEntidadsAsociar();
//            esNuevo = false;
//            txtNombre = EntidadSelected.getNombre();
//            txtDireccion = EntidadSelected.getDireccion();
//            refDepartamentoSelect = EntidadSelected.getRefMunicipio().getDivisionPoliticiaId().getId();
//            refMunicipioSelect = EntidadSelected.getRefMunicipio().getId();
//            if(EntidadSelected.getIdMiembro()!= null){
//                miembroPastorSelect = EntidadSelected.getIdMiembro().getId();
//            }
//            if(EntidadSelected.getIdEntidadPadre()!= null){
//                EntidadPadreSelect = EntidadSelected.getIdEntidadPadre().getId();
//            }
//            txtNombreCompleto= EntidadSelected.getIdMiembro().getNombreCompleto();
//
//
//            cargarMunicipio();
//            cargarMiembrosPastoresEntidad();
//
//            RequestContext context = RequestContext.getCurrentInstance();
//            context.execute("PF('dialogoRegistro').show();");
//        } else {
//            FacesUtils.addInfoMessage("Tiene que seleccionar un Registro!!");
        }


    }

    public void abrirDialogoAgregar() {
        limpiar();
        cargarDepartamento();
        cargarListaEntidadsAsociar();
        EntidadSelected = null;
        esNuevo = true;
        if(listaEntidad!=null){
            listaEntidad.clear();
        }

        prBuscar="";
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dialogoRegistro').show();");

    }

    public void guardar() throws DAOException {
//
//        if (esNuevo) {
//            if(!miembroCreado){
//                FacesUtils.addErrorMessage("El Pastor es obligatorio!!");
//                return;
//            }
//            Miembro oMiembro = new Miembro();
//            oMiembro.setNombreCompleto(txtNombreCompleto);
//            oMiembro.setEstadoCivil(catalogoService.obtenerCatalogoPorCodigo(cmbEstadoCivil));
//            oMiembro.setSexo(catalogoService.obtenerCatalogoPorCodigo(cmbsexo));
//            oMiembro.setTipoMiembro(catalogoService.obtenerCatalogoPorCodigo("001"));
//            oMiembro.setUsuarioRegistra(this.getUsuarioActual());
//            oMiembro.setFechaRegistra(GenericUtils.nowTimeStamp());
//            miembroService.guardar(oMiembro);
//
//            Entidad oEntidad = new Entidad();
//            oEntidad.setNombre(txtNombre);
//            oEntidad.setDireccion(txtDireccion);
//            oEntidad.setRefMunicipio(extraerMunicipioPorId());
//            oEntidad.setUsuarioRegistra(this.getUsuarioActual());
//            oEntidad.setFechaRegistra(GenericUtils.nowTimeStamp());
//            oEntidad.setIdEntidadPadre(extraerEntidadPadre());
//            oEntidad.setIdMiembro(oMiembro);
//            EntidadService.guardar(oEntidad);
//            oMiembro.setIdEntidad(oEntidad);
//            miembroService.actualizar(oMiembro);
//            FacesUtils.addInfoMessage("El Registro se Guardo Exitosamente!!");
//
//        } else {
//
//            if(miembroCreado){
//                Miembro oMiembro = new Miembro();
//                oMiembro.setNombreCompleto(txtNombreCompleto);
//                oMiembro.setEstadoCivil(catalogoService.obtenerCatalogoPorCodigo(cmbEstadoCivil));
//                oMiembro.setSexo(catalogoService.obtenerCatalogoPorCodigo(cmbsexo));
//                oMiembro.setTipoMiembro(catalogoService.obtenerCatalogoPorCodigo("001"));
//                oMiembro.setUsuarioRegistra(this.getUsuarioActual());
//                oMiembro.setFechaRegistra(GenericUtils.nowTimeStamp());
//                oMiembro.setIdEntidad(EntidadSelected);
//                miembroService.guardar(oMiembro);
//                EntidadSelected.setIdMiembro(oMiembro);
//            }
//
//            EntidadSelected.setNombre(txtNombre);
//            EntidadSelected.setDireccion(txtDireccion);
//            EntidadSelected.setRefMunicipio(extraerMunicipioPorId());
//            EntidadSelected.setUsuarioActualiza(this.getUsuarioActual());
//            EntidadSelected.setFechaModifica(GenericUtils.nowTimeStamp());
//            EntidadSelected.setIdEntidadPadre(extraerEntidadPadre());
//            EntidadService.actualizar(EntidadSelected);
//            FacesUtils.addInfoMessage("El Registro se actualizo Exitosamente!!");
//
//        }
//        limpiar();
//        desabilitarBotones = true;
//        RequestContext context = RequestContext.getCurrentInstance();
//        context.execute("PF('dialogoRegistro').hide();");


    }

    public void cargarDepartamento() {

        if (listaDepartamento == null) {
            listaDepartamento = divisisionPoliticaService.obtenerListaDepartamentos();
        }
    }

    public void cargarMunicipio() {
        listaMunicipio = divisisionPoliticaService.obtenerListaMunicipios(refDepartamentoSelect);
    }

    public DivisionPolitica extraerMunicipioPorId() {
        DivisionPolitica municipio = null;
        for (DivisionPolitica mun : listaMunicipio) {
            if (mun.getId() == refMunicipioSelect) {
                municipio = mun;
            }
        }
        return municipio;
    }

    public void limpiar() {
        esNuevo = true;
        txtNombre = "";
        txtDireccion = "";
        refDepartamentoSelect = 0;
        refMunicipioSelect = 0;
        if(listaMunicipio!=null){
            listaMunicipio.clear();
        }
        EntidadSelected = null;
        miembroPastorSelect=0;
        EntidadPadreSelect = 0;
        txtNombreCompleto="";
    }

    public void cargarListaEntidadsAsociar(){
        if(listaEntidadsAsociar==null){
        //    listaEntidadsAsociar = EntidadService.BuscarEntidad("");
        }
    }
    public void cargarMiembrosPastoresEntidad(){
    //    listaMiembrosPastor = miembroService.obtenerMiembrosPastoresPorEntidad(EntidadSelected.getId());
    }
    public void cargarListaEntidadsHijas(){
      //  listaEntidadsHijas = EntidadService.buscarEntidadHijasPorEntidadPadre(EntidadSelected.getId());
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dialogoEntidadH').show();");
    }

    private Entidad extraerEntidadPadre(){
        Entidad oEntidad = null;
        for(Entidad EntidadP : listaEntidadsAsociar){
            if(EntidadP.getId()==EntidadPadreSelect){
                oEntidad = EntidadP;
            }
        }
        return oEntidad;
    }

    public void crearMiembro(){
        miembroCreado = true;
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dialogoRegistroPastor').hide();");
    }
    public void limpiarDatosPastor(){
        txtNombreCompleto = "";
        cmbEstadoCivil="";
        cmbsexo="";

    }

    public void abrirModalPastor(){
        cargarEstadoCivil();
        cargarSexo();
        limpiarDatosPastor();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dialogoRegistroPastor').show();");
    }

    public void cargarEstadoCivil(){
        if(listaEstadoCivil== null){
            listaEstadoCivil = catalogoService.obtenerCatalogoPorTipoCatalogo("002");
        }
    }

    public void cargarSexo(){
        if(listaSexo== null){
            listaSexo = catalogoService.obtenerCatalogoPorTipoCatalogo("003");
        }
    }


    public String getPrBuscar() {
        return prBuscar;
    }

    public void setPrBuscar(String prBuscar) {
        this.prBuscar = prBuscar;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<Entidad> getListaEntidad() {
        return listaEntidad;
    }

    public void setListaEntidad(List<Entidad> listaEntidad) {
        this.listaEntidad = listaEntidad;
    }

    public Entidad getEntidadSelected() {
        return EntidadSelected;
    }

    public void setEntidadSelected(Entidad EntidadSelected) {
        this.EntidadSelected = EntidadSelected;
    }

    public boolean isDesabilitarBotones() {
        return desabilitarBotones;
    }

    public void setDesabilitarBotones(boolean desabilitarBotones) {
        this.desabilitarBotones = desabilitarBotones;
    }

    public boolean isEsNuevo() {
        return esNuevo;
    }

    public void setEsNuevo(boolean esNuevo) {
        this.esNuevo = esNuevo;
    }

    public String getTxtDireccion() {
        return txtDireccion;
    }

    public void setTxtDireccion(String txtDireccion) {
        this.txtDireccion = txtDireccion;
    }

    public int getRefDepartamentoSelect() {
        return refDepartamentoSelect;
    }

    public void setRefDepartamentoSelect(int refDepartamentoSelect) {
        this.refDepartamentoSelect = refDepartamentoSelect;
    }

    public int getRefMunicipioSelect() {
        return refMunicipioSelect;
    }

    public void setRefMunicipioSelect(int refMunicipioSelect) {
        this.refMunicipioSelect = refMunicipioSelect;
    }

    public List<DivisionPolitica> getListaDepartamento() {
        return listaDepartamento;
    }

    public void setListaDepartamento(List<DivisionPolitica> listaDepartamento) {
        this.listaDepartamento = listaDepartamento;
    }

    public List<DivisionPolitica> getListaMunicipio() {
        return listaMunicipio;
    }

    public void setListaMunicipio(List<DivisionPolitica> listaMunicipio) {
        this.listaMunicipio = listaMunicipio;
    }

    public String getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(String txtNombre) {
        this.txtNombre = txtNombre;
    }

    public List<Miembro> getListaMiembrosPastor() {
        return listaMiembrosPastor;
    }

    public void setListaMiembrosPastor(List<Miembro> listaMiembrosPastor) {
        this.listaMiembrosPastor = listaMiembrosPastor;
    }

    public List<Entidad> getListaEntidadsHijas() {
        return listaEntidadsHijas;
    }

    public void setListaEntidadsHijas(List<Entidad> listaEntidadsHijas) {
        this.listaEntidadsHijas = listaEntidadsHijas;
    }

    public List<Entidad> getListaEntidadsAsociar() {
        return listaEntidadsAsociar;
    }

    public void setListaEntidadsAsociar(List<Entidad> listaEntidadsAsociar) {
        this.listaEntidadsAsociar = listaEntidadsAsociar;
    }

    public int getEntidadPadreSelect() {
        return EntidadPadreSelect;
    }

    public void setEntidadPadreSelect(int EntidadPadreSelect) {
        this.EntidadPadreSelect = EntidadPadreSelect;
    }

    public int getMiembroPastorSelect() {
        return miembroPastorSelect;
    }

    public void setMiembroPastorSelect(int miembroPastorSelect) {
        this.miembroPastorSelect = miembroPastorSelect;
    }

    public String getTxtNombreCompleto() {
        return txtNombreCompleto;
    }

    public void setTxtNombreCompleto(String txtNombreCompleto) {
        this.txtNombreCompleto = txtNombreCompleto;
    }

    public String getCmbsexo() {
        return cmbsexo;
    }

    public void setCmbsexo(String cmbsexo) {
        this.cmbsexo = cmbsexo;
    }

    public String getCmbEstadoCivil() {
        return cmbEstadoCivil;
    }

    public void setCmbEstadoCivil(String cmbEstadoCivil) {
        this.cmbEstadoCivil = cmbEstadoCivil;
    }

    public Catalogo getTipoMiembro() {
        return tipoMiembro;
    }

    public void setTipoMiembro(Catalogo tipoMiembro) {
        this.tipoMiembro = tipoMiembro;
    }

    public List<Catalogo> getListaEstadoCivil() {
        return listaEstadoCivil;
    }

    public void setListaEstadoCivil(List<Catalogo> listaEstadoCivil) {
        this.listaEstadoCivil = listaEstadoCivil;
    }

    public List<Catalogo> getListaSexo() {
        return listaSexo;
    }

    public void setListaSexo(List<Catalogo> listaSexo) {
        this.listaSexo = listaSexo;
    }

    public boolean isMiembroCreado() {
        return miembroCreado;
    }

    public void setMiembroCreado(boolean miembroCreado) {
        this.miembroCreado = miembroCreado;
    }
}
