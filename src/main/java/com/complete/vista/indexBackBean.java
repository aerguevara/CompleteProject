package com.complete.vista;

import com.complete.modelo.entity.seguridad.Usuario;
import com.complete.utils.BaseBean;
import com.complete.utils.FacesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Angelo on 15/09/2016.
 */
@Named
@Scope("view")
public class indexBackBean extends BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String paginaActual;
    private String nombreUsuarioActual;

    @Autowired
    @Qualifier("loginBackBean")
    loginBackBean loginsesion;

    @PostConstruct
    private void init(){
        if(loginsesion.getPaginaActual()!=null){
            this.paginaActual= loginsesion.getPaginaActual();
        }
    }

    public String getNombreUsuarioActual() {
        nombreUsuarioActual = this.getUsuarioActual().getNombreCompleto();
        return nombreUsuarioActual;
    }

    public void setNombreUsuarioActual(String nombreUsuarioActual) {
        this.nombreUsuarioActual = nombreUsuarioActual;
    }

    public void pagina(String pagina){
        if("views/Iglesia.xhtml".equalsIgnoreCase(pagina)){
            if( "ADMINISTRADOR".equalsIgnoreCase(this.getRolUsuarioActual())){
                loginsesion.setPaginaActual(pagina);
            }else{
                FacesUtils.addInfoMessage("Usted no Tiene Permisos para Acceder al Recurso");
            }

        }else{
            loginsesion.setPaginaActual(pagina);
        }

        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        ViewHandler handler = context.getApplication().getViewHandler();
        UIViewRoot root = handler.createView(context, viewId);
        root.setViewId(viewId);
        context.setViewRoot(root);


    }
    public String getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(String paginaActual) {
        this.paginaActual = paginaActual;
    }


}
